package com.bigpi.dsp.kemanager.fhe.key.models;

public class Multiplicity {
	public Multiplicity(String ip, Integer list_size) {
		super();
		this.ip = ip;
		this.list_size = list_size;
	}
	public Multiplicity() {
		this.ip = "192.168.0.1";
		this.list_size = 10;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Integer getList_size() {
		return list_size;
	}
	public void setList_size(Integer list_size) {
		this.list_size = list_size;
	}
	private String ip;
	private Integer list_size;
}
