package com.bigpi.dsp.kemanager.fhe.key.helper;

import java.io.File;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.spring.web.paths.Paths;

@SuppressWarnings("unused")
@Configuration
public class Constants {
	
	public static final String SEPARATOR_PARAM 	= "_TRANS_";
	/*BASE PRODUCTION*/	
	public static final String USR_BASE 			= "/opt";
	/*LOCAL BASE*/
	//public static final String USR_BASE = "/home/nguyen";
	public static final String FHE_BASE 			= USR_BASE + File.separator + "FHE_CIN";
	public static final String FHE_REPOSITORY_DIR 	= USR_BASE + File.separator + "FHE_C3ISP" + File.separator + "repository";
	/*BASE LOCAL*/	
	public static final String FHE_ALGO_DIR 		= FHE_BASE + File.separator + "ALGO"; 	
	public static final String FHE_PREPAREDATA_DIR 	= FHE_BASE + File.separator + "C3ISP" + File.separator + "prepareData";
	
	public static class FheScripts
	{
		public static final String FHE_GENERATE_KEYS = FHE_BASE + File.separator + "generate_for_java.sh";
	}
	
	public static class FheAlgo
	{
		public static final String BLACKLIST_FULL_NAME 		= "BLACK_LIST_FULL";
		public static final String BLACKLIST_HIGH_NAME 		= "BLACK_LIST_HIGH";
		public static final String BLACKLIST_MEDIUM_NAME 	= "BLACK_LIST_MEDIUM";
		public static final String BLACKLIST_LOW_NAME 		= "BLACK_LIST_LOW";
		public static final String BLACKLIST_DIR = FHE_ALGO_DIR + File.separator + "%s";
	}
	
	public class FheKeys
	{
		public static final String FHE_PATH_VAULT = "secret/"; 
		public static final String FHE_EVAL_FILE = "fhe_key.evk";
		public static final String FHE_SK_FILE 	= "fhe_key.sk";
		public static final String FHE_PK_FILE 	= "fhe_key.pk";
		public static final String FHE_PARAM_FILE = "fhe_params.xml";
	}
	
	public class FheConfigKeys
	{
		public static final String FHE_EVAL_SUFFIX 	= "evk";
		public static final String FHE_SK_SUFFIX 	= "sk";
		public static final String FHE_PK_SUFFIX 	= "pk";
		public static final String FHE_PARAM_SUFFIX = "xml";
		public static final String FHE_VERSION_SUFFIX = "ver";
		public static final String FHE_VERSION_VALUE = "value";
	}
	
	public class DefaultParam
	{
		public static final String REQUEST_ID_PARAM 	= "requestID";
		public static final String PAYLOAD_FORMAT_PARAM = "payloadFormat";		
		public static final String CONTENT_FILE_PARAM 	= "fileContent";
		public static final String ENCRYPTED_FIELD_PARAM = "encryptedField";
		public static final String OFFSET_KEYSTREAM_PARAM = "offsetTranscrypting";
	}
	
	public class EventHandleParam
	{		
		public static final String METADATA_FILE_PARAM 	= "metadataFile";
		public static final String CTI_FILE_PARAM 		= "ctiFile";
		public static final String DSA_ID_PARAM 		= "dsaId";
		public static final String DPOS_ID_PARAM 		= "dposId";		
	}
	
	public class BundleResponseParam
	{				
		public static final String FILE_NAME_PARAM 		= "fileName";
		public static final String STATUS_RESULT_PARAM 	= "result";	
	}
	
	public class C3ispComponents
	{
		public static final String ISI_NODE 			= "isic3isp";
		public static final String CSS_NODE	 			= "cssc3isp";		
		public static final String ISI_BM_COMPONENT 	= "isibmcom";
		public static final String CSS_DPOS_COMPONENT	= "cssdposcom";
	}
	
	public class BundleManagerSuffixFile
	{
		public static final String METADATA_SUFFIX 	= ".head";
		public static final String CTI_SUFFIX 		= ".payload";
		public static final String DSA_SUFFIX 		= ".dsa";
		public static final String HASH_SUFFIX 		= ".sign";
	}
	
	public class FHEModel
	{
		public static final String FHE_CONN_MALIC 	= "fhe_conn_malic";
	}
}
