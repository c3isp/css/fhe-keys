package com.bigpi.dsp.kemanager.fhe.key.models;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;


public class KeyTransfer {
	
	@JsonProperty("dsaID")
	private String dsaID;
	
	@JsonProperty("requestID")
	private String requestID; 
	
	@JsonProperty("lstKeys")
	private List<KeyObject> lstKeys;

	public KeyTransfer() {
		super();
		this.lstKeys = new ArrayList<KeyObject>();
	}
	
	public KeyTransfer(String dsaID, String requestID) {
		this();
		this.dsaID = dsaID;
		this.requestID = requestID;
	}
		
	public KeyTransfer(String dsaID, String requestID, List<KeyObject> lstKeys) {
		this(dsaID, requestID);
		this.lstKeys = lstKeys;
	}		

	public String getDsaID() {
		return dsaID;
	}
	public void setDsaID(String dsaID) {
		this.dsaID = dsaID;
	}
	public String getRequestID() {
		return requestID;
	}
	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}
	public List<KeyObject> getLstKeys() {
		return lstKeys;
	}
	public void setLstKeys(List<KeyObject> lstKeys) {
		this.lstKeys = lstKeys;
	}
	
	public void addKeyObject(KeyObject kO)
	{
		this.lstKeys.add(kO);
	}
}
