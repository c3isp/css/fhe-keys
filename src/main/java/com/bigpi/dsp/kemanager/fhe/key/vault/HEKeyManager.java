package com.bigpi.dsp.kemanager.fhe.key.vault;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.vault.core.VaultTemplate;
import org.springframework.vault.support.VaultResponseSupport;

import com.bigpi.dsp.kemanager.fhe.key.helper.Constants;
import com.bigpi.dsp.kemanager.fhe.key.vault.io.*;
import com.google.common.io.Files;


/**
 * This class is only applied for CNR Server and in C3ISP Project
 * */
public class HEKeyManager {
	private static Logger log = LoggerFactory.getLogger(HEKeyManager.class);

	/**
	 * This class encapsulates the operations for HE key management from Vault.
	 *
	 * @author Quentin Lutz
	 */
	private String m_token;
	private VaultTemplate m_temKeyValue;

	/**
	 * Create a new {@link HEKeyManager} with a {@link VaultTemplate}.
	 *
	 * @param transit must not be {@literal null}.
	 */
	public HEKeyManager(String token, VaultTemplate template) {
		this.m_temKeyValue = template;
		this.m_token = token;
	}

	/**
	 * Create a new HE key triplet.
	 *
	 * @param userID is the ID for the active user.
	 * @param modelID is the ID for the HE model analysis.
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public boolean CreateHETriplet(String userID, String modelID)
			throws IOException, InterruptedException {
		
		if(new File(Constants.FheScripts.FHE_GENERATE_KEYS).exists() == false)
			throw new IOException("File " + Constants.FheScripts.FHE_GENERATE_KEYS + " is not existed !");
		
		int ver = GetVersion(userID, modelID);
		String nextVer = Integer.toString(ver+1);
		String keyId = BuildKeyId(ver+1, userID, modelID);
		String usable_keyId = keyId.replaceAll(":", "\\:");
		String verId = BuildVerId(userID,modelID);
		
		String fheUserDir = Constants.FHE_BASE + File.separator + modelID + File.separator + userID;
		String fheParamFile = "";
		log.info("FHE Model " + modelID);
		fheParamFile = String.format(Constants.FheAlgo.BLACKLIST_DIR, modelID) + File.separator + Constants.FheKeys.FHE_PARAM_FILE;
		
		if(new File(fheParamFile).exists() == false)
			throw new IOException("No model was found !");
		
		log.info("Execution for generating keys now ! ");
		boolean done = BashTools.Exec(true, Constants.FheScripts.FHE_GENERATE_KEYS,
				fheParamFile, usable_keyId, fheUserDir);
		if (done) {
			log.info("Done ! Generating & store Keys for keyId = " + keyId + " and version " + nextVer);	
			log.info(String.format("%s %s %s %s\n", 
								Constants.FheScripts.FHE_GENERATE_KEYS,
								fheParamFile, usable_keyId, fheUserDir));
			
		}
		else 
			throw new IOException("Error Cannot execute for generating keys !");
		
		Map<String,String> keyValues = new HashMap<String,String>();
		/**
		 *  Vault cannot working with binary array: 
		 *  Caller to encode binary data as base64 before sending it in via the CLI, 
		 *  just like it's up to API users to base64 their data encoded in their POST calls.
		 * */
		keyValues.put(Constants.FheConfigKeys.FHE_VERSION_SUFFIX, 	nextVer);
		keyValues.put(Constants.FheConfigKeys.FHE_EVAL_SUFFIX, 		
				Base64.getEncoder().encodeToString(FileOperations.GetData(
						fheUserDir + File.separator + usable_keyId + "."+Constants.FheConfigKeys.FHE_EVAL_SUFFIX)));
		keyValues.put(Constants.FheConfigKeys.FHE_SK_SUFFIX, 		
				Base64.getEncoder().encodeToString(FileOperations.GetDataAndShred(
						fheUserDir + File.separator + usable_keyId + "."+Constants.FheConfigKeys.FHE_SK_SUFFIX)));
		keyValues.put(Constants.FheConfigKeys.FHE_PK_SUFFIX, 				
				Base64.getEncoder().encodeToString(FileOperations.GetData(
						fheUserDir + File.separator + usable_keyId + "."+Constants.FheConfigKeys.FHE_PK_SUFFIX)));
		keyValues.put(Constants.FheConfigKeys.FHE_PARAM_SUFFIX, 
				Base64.getEncoder().encodeToString(FileOperations.GetData(fheParamFile)));
		
		m_temKeyValue.write(Constants.FheKeys.FHE_PATH_VAULT + verId, 
				Collections.singletonMap(Constants.FheConfigKeys.FHE_VERSION_VALUE,nextVer));
		m_temKeyValue.write(Constants.FheKeys.FHE_PATH_VAULT + keyId, keyValues);
		
		return true; 
	}

	public void DestroyFHEKeys(String userID, String modelID) throws NullPointerException, IOException, InterruptedException
	{
		int ver = GetVersion(userID, modelID);
		if(ver == 0) return;
		String keyId = BuildKeyId(ver, userID, modelID);		
		String verId = BuildVerId(userID,modelID);
		
		m_temKeyValue.delete(Constants.FheKeys.FHE_PATH_VAULT + verId);
		m_temKeyValue.delete(Constants.FheKeys.FHE_PATH_VAULT + keyId);
		
		System.out.println("Done ! Destroy FHE Keys for keyId = " + keyId + " and version " + verId);		
	}
	/**
	 * Return the values of a given HE key triplet.
	 *
	 * @param userID is the ID for the active user.
	 * @param modelID is the ID for the HE model analysis.
	 * @param version is the desired version
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	public ArrayList<byte[]> GetHETriplet(String userID, String modelID, int version) throws IOException, InterruptedException, NullPointerException {		
		if(__IsExisted(userID, modelID) ==  false )
		{
			CreateHETriplet(userID, modelID);
		}
		ArrayList<byte[]> keys = new ArrayList<byte[]>();
		int ver = GetVersion(userID, modelID);
		
		if (version>ver) {
			throw new IOException("No GetHETriplet key version " + version + " was existed !");
		}
		String keyId = BuildKeyId(version, userID, modelID);
		Collection<Object> response = m_temKeyValue.read(Constants.FheKeys.FHE_PATH_VAULT+keyId).getData().values();
		for (Object e: response) {
			keys.add( Base64.getDecoder().decode(((String)e).getBytes()) );
		}
		return keys;
	}

	/**
	 * Return the values of the last version
	 * of a given HE key triplet.
	 *
	 * @param userID is the ID for the active user.
	 * @param modelID is the ID for the HE model analysis.
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	public ArrayList<byte[]> GetHETriplet(String userID, String modelID) throws IOException, InterruptedException, NullPointerException {				
		int ver = GetVersion(userID, modelID);
		return GetHETriplet(userID, modelID, ver);
	}

	/**
	 * Return the public key of a given HE key triplet.
	 *
	 * @param userID is the ID for the active user.
	 * @param modelID is the ID for the HE model analysis.
	 * @param version is the desired version
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	public byte[] GetHEKey(String SuffixKey, String userID, String modelID,int version) throws IOException, InterruptedException, NullPointerException {		
		if(__IsExisted(userID, modelID) ==  false )
		{
			CreateHETriplet(userID, modelID);
		}		
		int ver = GetVersion(userID, modelID);		
		if (version > ver || ver == 0) {
			throw new IOException("No GetHEPublicKey key version " + version + " was existed !");
		}
		String keyId = BuildKeyId(version,userID,modelID);
		Map<String, Object> keyValues = m_temKeyValue.read(Constants.FheKeys.FHE_PATH_VAULT+keyId).getData();
		assert(keyValues != null);
		assert(keyValues.size() > 0);
				
		return Base64.getDecoder().decode( ((String) keyValues.get(SuffixKey)).getBytes() );
	}

	/**
	 * Return the last version of the
	 * public key of a given HE key triplet.
	 *
	 * @param userID is the ID for the active user.
	 * @param modelID is the ID for the HE model analysis.
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	public byte[] GetHEPublicKey(String userID, String modelID) throws IOException, InterruptedException, NullPointerException {				
		int ver = GetVersion(userID, modelID);
		return GetHEKey(Constants.FheConfigKeys.FHE_PK_SUFFIX, userID, modelID, ver);
	}

	/**
	 * Return the last version of the
	 * secret key of a given HE key triplet.
	 *
	 * @param userID is the ID for the active user.
	 * @param modelID is the ID for the HE model analysis.
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	public byte[] GetHESecretKey(String userID, String modelID) throws IOException, InterruptedException, NullPointerException {				
		int ver = GetVersion(userID, modelID);
		return GetHEKey(Constants.FheConfigKeys.FHE_SK_SUFFIX, userID, modelID, ver);
	}

	/**
	 * Return the last version of the
	 * evaluation key of a given HE key triplet.
	 *
	 * @param userID is the ID for the active user.
	 * @param modelID is the ID for the HE model analysis.
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	public byte[] GetHEEvaluationKey(String userID, String modelID) throws IOException, InterruptedException, NullPointerException {
		int ver = GetVersion(userID, modelID);
		return GetHEKey(Constants.FheConfigKeys.FHE_EVAL_SUFFIX, userID, modelID, ver);
	}
	
	
	/**
	 * Return the last version of the
	 * parameter key of a given HE key triplet.
	 *
	 * @param userID is the ID for the active user.
	 * @param modelID is the ID for the HE model analysis.
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	public byte[] GetHEParameter(String userID, String modelID) throws IOException, InterruptedException, NullPointerException {
		int ver = GetVersion(userID, modelID);
		return GetHEKey(Constants.FheConfigKeys.FHE_PARAM_SUFFIX, userID, modelID, ver);
	}

	/**
	 * Return the last version number of a
	 * given HE key triplet.
	 *
	 * @param userID is the ID for the active user.
	 * @param modelID is the ID for the HE model analysis.
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	public int GetVersion(String userID, String modelID) throws IOException, InterruptedException, NullPointerException {		
		boolean exists = __IsExisted(userID, modelID);
		String versionId =  BuildVerId(userID,modelID);
		if (exists) {			
			VaultResponseSupport<HashMap> response = m_temKeyValue.read(Constants.FheKeys.FHE_PATH_VAULT + versionId, HashMap.class);
			if(response == null) throw new IOException ("Please check Creating FHE Keys process ! or Vault Instance");
			return Integer.valueOf((String)response.getData().get(Constants.FheConfigKeys.FHE_VERSION_VALUE) );
		} else {
			return 0;
		}
	}

	private boolean __IsExisted(String userID, String modelID) {		
		return m_temKeyValue.read(Constants.FheKeys.FHE_PATH_VAULT+BuildVerId(userID,modelID))!=null;
	}

	/**
	 * Return the ID to be used in Vault
	 * for the given parameters to check
	 * key values.
	 *
	 * @param ver is the version number for the desired key.
	 * @param userID is the ID for the active user.
	 * @param modelID is the ID for the HE model analysis.
	 */
	public String BuildKeyId(int ver, String userID, String modelID) {
		return ("v"+Integer.toString(ver)+":HE:"+modelID+":"+userID);
	}

	/**
	 * Return the ID to be used in Vault
	 * for the given parameters to check
	 * key versions.
	 *
	 * @param userID is the ID for the active user.
	 * @param modelID is the ID for the HE model analysis.
	 */
	public String BuildVerId(String userID, String modelID) {
		return modelID+":"+userID;
	}


	/**
	 * Return the path to the XML file
	 * containing the parameters for the
	 * given model ID.
	 * 
	 * @param modelID is the ID for the HE model analysis.
	 */
	public String GetParamsXML(String modelID) {
		//TODO Add the relevant body
		String homeDir = Constants.FHE_ALGO_DIR;
		return homeDir + File.separator + modelID + File.separator + Constants.FheKeys.FHE_PARAM_FILE;
	}

}