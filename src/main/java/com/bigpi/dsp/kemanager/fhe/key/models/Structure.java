/**
 *  Copyright 2017 Hewlett Packard Enterprise Development Company, L.P.
 */
package com.bigpi.dsp.kemanager.fhe.key.models;

public class Structure {

	/**
	 * A generic structure that will be used by the template interface.
	 * It can be automatically parsed as JSON by Springboot
	 */
	String name;
	String id;
	String path;
	String version;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}
