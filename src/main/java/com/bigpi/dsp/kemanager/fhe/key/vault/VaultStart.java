package com.bigpi.dsp.kemanager.fhe.key.vault;

import java.net.URI;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.vault.VaultException;
import org.springframework.vault.authentication.TokenAuthentication;
import org.springframework.vault.client.VaultEndpoint;
import org.springframework.vault.core.VaultSysTemplate;
import org.springframework.vault.core.VaultTemplate;
import org.springframework.vault.core.VaultTransitTemplate;
import org.springframework.vault.support.VaultMount;


/**
 * This class encapsulates the operations for the connection to the Vault server.
 *
 * @author Quentin Lutz
 */
public class VaultStart {

	Logger log = LoggerFactory.getLogger(VaultStart.class);
	
	public VaultStart(String token) {
		super();
		this.token = token;
	}


	/**
	 * Create a new {@link VaultTransitTemplate} from a token.
	 *
	 */
	private String token;

	public VaultTransitTemplate GetTransit(String vaultUri) {		
		
		try {
			VaultTemplate vaultTemplate = GetTemplate(vaultUri);

			VaultSysTemplate system = new VaultSysTemplate(vaultTemplate);
			Map<String,VaultMount> mounts = system.getMounts();
			if (!mounts.containsKey("transit/")) {
				system.mount("transit", VaultMount.create("transit"));
			}

			VaultTransitTemplate transit = (VaultTransitTemplate) vaultTemplate.opsForTransit("transit");			
			return transit;
		} catch (VaultException e) {	
			log.error("Vault exception " + e.getMessage());
			e.printStackTrace();
		}
		log.error("Vault Error some where ");
		return null;
	}


	public VaultTemplate GetTemplate(String vaultUri) {
		return new VaultTemplate(VaultEndpoint.from(URI.create(vaultUri)),new TokenAuthentication(token));
	}

}
