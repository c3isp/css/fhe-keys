package com.bigpi.dsp.kemanager.fhe.key.models;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class Shell {
  public static void shell(String[] argv) throws Exception {   
	List<String> cmd = new ArrayList<String> () ; 
    cmd.add("/bin/sh"); 
    cmd.add("-c"); 
//    cmd.add("pwd"); 
    cmd.add(argv[0]);	
    cmd.add(String.valueOf(argv.length));
    ProcessBuilder builder = new ProcessBuilder(cmd);
    final Process process = builder.start();
    InputStream is = process.getInputStream();
    InputStreamReader isr = new InputStreamReader(is);
    BufferedReader br = new BufferedReader(isr);
    String line;
    while ((line = br.readLine()) != null) {
      System.out.println(line);
    }
    System.out.println("Program terminated!");
  }
}	




