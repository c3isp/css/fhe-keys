package com.bigpi.dsp.kemanager.fhe.key.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Tools {
	static Logger log = LoggerFactory.getLogger(Tools.class);	

	public static String encodeHmacSHA(String key, byte[] data) {
	    try {

	        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
	        SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
	        sha256_HMAC.init(secret_key);

	        return new String(Hex.encodeHex(sha256_HMAC.doFinal(data)));	    
	    } catch (InvalidKeyException e) {
	        e.printStackTrace();
	    } catch (NoSuchAlgorithmException e) {
	        e.printStackTrace();
	    } catch (UnsupportedEncodingException e) {
	        e.printStackTrace();
	    }
	    return null;
	}
	
	public static String encodeFileHmacSHA(String key, File initialFile)
	{
		try {
			return encodeFileHmacSHA(key, new FileInputStream(initialFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}		
	
	public static String encodeFileHmacSHA(String key, InputStream in) throws java.io.IOException
	{
		try {
			Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
			SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
			sha256_HMAC.init(secret_key);
			
			byte[] ibuf = new byte[1024];
			int len;
			while ((len = in.read(ibuf)) != -1) {
				sha256_HMAC.update(ibuf, 0, len);
			}
			return new String(Hex.encodeHex(sha256_HMAC.doFinal()));
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static File createTempFile(String fileName, String suffix, String fileObjectContent)
			throws IOException, FileNotFoundException {
		// Since Java 1.7 Files and Path API simplify operations on files
		Path path = Files.createTempFile(fileName, suffix);
        File file = path.toFile();
        // writing sample data
        Files.write(path, fileObjectContent.getBytes(StandardCharsets.UTF_8));
        // This tells JVM to delete the file on JVM exit.
        // Useful for temporary files in tests.
        file.deleteOnExit();
        return file;
	}    
	
	public static File createTempFile(String fileName, String suffix)
			throws IOException, FileNotFoundException {
		// Since Java 1.7 Files and Path API simplify operations on files
		Path path = Files.createTempFile(fileName, suffix);
        File file = path.toFile();
        // This tells JVM to delete the file on JVM exit.
        // Useful for temporary files in tests.
        file.deleteOnExit();
        return file;
	}    
	
	public static File createTempFile(String fileName, String suffix, 
			byte[] fileObjectContent) throws IOException {
		return createTempFile(fileName, suffix, 
				new String(fileObjectContent, StandardCharsets.UTF_8));
	}
	
	public static byte[] ZipContent(List<File> srcFiles, String fileName) throws FileNotFoundException, IOException {
		File file = __zipFile(srcFiles, fileName);
		Path path = Paths.get(file.getAbsolutePath());
		byte[] data = null;
		data = Files.readAllBytes(path);
		return data;
	}
	
	private static File __zipFile(List<File> srcFiles, String fileName) throws FileNotFoundException, IOException {
		File file = Tools.createTempFile(fileName, ".zip");
		FileOutputStream   fos = new FileOutputStream(file);
		ZipOutputStream zos = new ZipOutputStream(fos);
		byte[] buffer = new byte[128];
		for (File currentFile : srcFiles) {
			if (!currentFile.isDirectory()) {
				ZipEntry entry = new ZipEntry(currentFile.getName());
				FileInputStream fis = new FileInputStream(currentFile);
				zos.putNextEntry(entry);
				int read = 0;
				while ((read = fis.read(buffer)) != -1) {
					zos.write(buffer, 0, read);
				}
				zos.closeEntry();
				fis.close();
			}
		}
		zos.close();
		fos.close();
		return file;
	}
	
}
