package com.bigpi.dsp.kemanager.fhe.key.restapi;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bigpi.dsp.kemanager.fhe.key.helper.Tools;
import com.bigpi.dsp.kemanager.fhe.key.models.KeyObject;
import com.bigpi.dsp.kemanager.fhe.key.models.KeyTransfer;
import com.bigpi.dsp.kemanager.fhe.key.vault.HEKeyManager;
import com.bigpi.dsp.kemanager.fhe.key.vault.KVKeyManager;
import com.bigpi.dsp.kemanager.fhe.key.vault.VaultStart;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiParam;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-03-16T11:13:18.135Z")

@RestController
@RequestMapping("/v1")
public class FheApiController implements FheApi {

	private final ObjectMapper objectMapper;

	private final HttpServletRequest request;

 	@Value("${spring.application.skmsg}")	
	public String SECRET_KEY_MSG_AUTHENTICATION;// = "78b4c2c6c7808000923e9f23c6dad1434cc53f6da99c2c5f2bcec4d6400198d5";
    
    @Value("${token.vault.c3isp.cnr.it}")
    private String TokenRoot;
    
    @Value("${uri.vault.c3isp.cnr.it}")
	private String vaultURI;


	@Autowired
	public FheApiController(ObjectMapper objectMapper, HttpServletRequest request) {
		this.objectMapper = objectMapper;
		this.request = request;
	}

	@Override
	public Optional<ObjectMapper> getObjectMapper() {
		return Optional.ofNullable(objectMapper);
	}

	@Override
	public Optional<HttpServletRequest> getRequest() {
		return Optional.ofNullable(request);
	}

	@Override
	public ResponseEntity<String> createKey(
			@ApiParam(value = "",required=true) @PathVariable("RequestId") String requestId,
			@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId,
			@ApiParam(value = "",required=true, allowableValues = FHE_ALGORITHM) @PathVariable("FHEModel") String fhEModel) {
		if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
			try {
				log.info("ResponseEntity<ServerResponse> createKey method start working");
				HEKeyManager HEKeyManager = new HEKeyManager(TokenRoot, new VaultStart(TokenRoot).GetTemplate(vaultURI));
				HEKeyManager.CreateHETriplet(dsAId, fhEModel);
				String message ="FHE key triplet created";
				return new ResponseEntity<String>(message, HttpStatus.CREATED);
			} catch (IOException e) {
				log.error("Couldn't serialize response for content type application/json", e);
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			} catch (InterruptedException e) {
				log.error("Couldn't create keys for the given parameters", e);
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			log.warn("ObjectMapper or HttpServletRequest not configured in default FheApi interface so no example is generated");
		}
		return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
	}


	@Override
	public ResponseEntity<KeyTransfer> getKey(@ApiParam(value = "",required=true, allowableValues = "FHE_PUBKEY, FHE_SECRETKEY, FHE_ALL") 
		@PathVariable("KeyType") String keyType,
		@ApiParam(value = "",required=true) @PathVariable("RequestId") String requestId,
		@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId,
		@ApiParam(value = "",required=true, allowableValues = FHE_ALGORITHM) @PathVariable("FHEModel") String fhEModel) {
		try {
			if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {				
				log.info("ResponseEntity<KeyTransfer> getKey method start working");
				KeyTransfer lstKeys = new KeyTransfer(dsAId, requestId);
				
				HEKeyManager keyMng = new HEKeyManager(TokenRoot, new VaultStart(TokenRoot).GetTemplate(vaultURI));
				byte[] keyContent;
				if ((keyType.compareTo("FHE_PUBKEY")==0)||(keyType.compareTo("FHE_ALL")==0)) 
				{						
					keyContent = keyMng.GetHEPublicKey(dsAId, fhEModel);						
					lstKeys.addKeyObject(new KeyObject().keyInfo(KeyObject.KeyInfoEnum.FHE_PK)
							.key(keyContent)
							.requestId(requestId)
							.checksum(Tools.encodeHmacSHA(SECRET_KEY_MSG_AUTHENTICATION, keyContent)));
					
					keyContent =keyMng.GetHEEvaluationKey(dsAId, fhEModel);
					lstKeys.addKeyObject(new KeyObject().keyInfo(KeyObject.KeyInfoEnum.FHE_EVK)
						.key(keyContent)
						.requestId(requestId)
						.checksum(Tools.encodeHmacSHA(SECRET_KEY_MSG_AUTHENTICATION, keyContent)));
				} 
				if ((keyType.compareTo("FHE_SECRETKEY")==0)||(keyType.compareTo("FHE_ALL")==0)) 
				{
					keyContent =keyMng.GetHESecretKey(dsAId, fhEModel);
					lstKeys.addKeyObject(new KeyObject().keyInfo(KeyObject.KeyInfoEnum.FHE_SK)
							.key(keyContent)
							.requestId(requestId)
							.checksum(Tools.encodeHmacSHA(SECRET_KEY_MSG_AUTHENTICATION, keyContent)));
				} 
				keyContent =keyMng.GetHEParameter(dsAId, fhEModel);
				lstKeys.addKeyObject(new KeyObject().keyInfo(KeyObject.KeyInfoEnum.FHE_PARAM)
						.key(keyContent)
						.requestId(requestId)
						.checksum(Tools.encodeHmacSHA(SECRET_KEY_MSG_AUTHENTICATION, keyContent)));
				
				return new ResponseEntity<KeyTransfer>(lstKeys, HttpStatus.OK);
				
			} else {
				log.warn("ObjectMapper or HttpServletRequest not configured in default FheApi interface so no example is generated");
			}
		} catch (NullPointerException | IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
	}  

	@Override
	public ResponseEntity<String> createTransKey(
			@ApiParam(value = "",required=true) @PathVariable("RequestId") String requestId,
			@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
            if (getAcceptHeader().get().contains("application/json")) {
            	
            	(new KVKeyManager(new VaultStart(TokenRoot).GetTemplate(vaultURI))).CreateKVKey(dsAId);
            	
                return new ResponseEntity<String>("Kreyvium key created", HttpStatus.OK);
            }
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default FheApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
	
	@Override
	public ResponseEntity<List<KeyObject>> getTransKey(
			@ApiParam(value = "",required=true) @PathVariable("RequestId") String requestId,
			@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
            if (getAcceptHeader().get().contains("application/json")) {
            	
				List<KeyObject> lstKeys = new ArrayList<KeyObject>();
				KVKeyManager keyMng = new KVKeyManager(new VaultStart(TokenRoot).GetTemplate(vaultURI));
				byte[] key = keyMng.GetKVKey(dsAId);				
				lstKeys.add(new KeyObject().keyInfo(KeyObject.KeyInfoEnum.TRANS_SK)
						.key(key)
						.requestId(requestId)
						.checksum(Tools.encodeHmacSHA(SECRET_KEY_MSG_AUTHENTICATION, key)));
                return new ResponseEntity<List<KeyObject>>(lstKeys, HttpStatus.OK);
            }
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default FheApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

	@Override
	public ResponseEntity<String> isExistedKey(
			@ApiParam(value = "",required=true) @PathVariable("RequestId") String requestId,
			@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId,
			@ApiParam(value = "",required=true, allowableValues = FHE_ALGORITHM) @PathVariable("FHEModel") String fhEModel) {
		if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
			try {
				log.info("ResponseEntity<ServerResponse> isExistedKey method start working");
				HEKeyManager HEKeyManager = new HEKeyManager(TokenRoot, new VaultStart(TokenRoot).GetTemplate(vaultURI));
				int version = HEKeyManager.GetVersion(dsAId, fhEModel);
				log.info("Version Keys " + version);
				if(version > 0) 
				{					
					return new ResponseEntity<String>("EXISTED", HttpStatus.CREATED);
				}
				else 
					return new ResponseEntity<String>("NOT FOUND", HttpStatus.NO_CONTENT);
			} catch (IOException e) {
				log.error("Couldn't serialize response for content type application/json", e);
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			} catch (InterruptedException e) {
				log.error("Couldn't create keys for the given parameters", e);
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		else {
			log.warn("ObjectMapper or HttpServletRequest not configured in default FheApi interface so no example is generated");
		}
		return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
	}
}
