package com.bigpi.dsp.kemanager.fhe.key.vault.io;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

import org.apache.commons.io.FilenameUtils;

/**
 * This class encapsulates the conversions of files into byte arrays and vice-versa.
 *
 * @author Quentin Lutz
 */
public class FileOperations {
	
	/**
	 * Extract bytes from a given file. only apply for Vault getkeys 
	 *  Caller to encode binary data as base64 before sending it in via the CLI, 
	 *  just like it's up to API users to base64 their data encoded in their POST calls.
	 * @param filePath , the path to the file
	 */
	public static byte[] GetData(String filePath) {
		Path path = Paths.get(filePath);
		try {
			byte[] data = Files.readAllBytes(path);			
			return data;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Print bytes into a given file
	 *
	 * @param filePath , the path to the file
	 * @param data , the data to print
	 */
	public static void PrintData(String filePath, byte[] data) {
		
		Path path = Paths.get(filePath);
		File file = new File(path.toString());
		String parentFolder = FilenameUtils.getFullPathNoEndSeparator(file.getAbsolutePath());
		BashTools.Exec(false, false, "mkdir", "-p", parentFolder, "", "", "", "", "", "" );
	    try {
			Files.write(path, data);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Shred a given file.
	 *
	 * @param filePath , the path to the file
	 */
	public static void Shred(String filePath) throws IOException, InterruptedException {
		BashTools.Exec(false, "shred", "-v", filePath, "", "", "", "", "", "");
		BashTools.Exec(false, "rm", filePath, "", "", "", "", "", "", "" );
	}
	
	/**
	 * Extract bytes from a given file then shred it.
	 *
	 * @param filePath , the path to the file
	 */
	public static byte[] GetDataAndShred(String filePath) throws IOException, InterruptedException {
		byte[] data = GetData(filePath);		
		BashTools.Exec(false, false, "shred", "-v", filePath, "", "", "", "", "", "");
		BashTools.Exec(false, false, "rm", filePath, "", "", "", "", "", "", "" );
		return data;
	}
	
}
